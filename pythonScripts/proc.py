#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# senmodel 
# acusticas python test.py --proto deploy.prototxt --model snapshot_iter_7260.caffemodel --meanfile mean.binaryproto --labelfile test.txt 
# alexnet python test.py --proto deploy.prototxt --model snapshot_iter_3660.caffemodel --meanfile mean.binaryproto --labelfile test.txt

import sys 
import caffe 
from caffe.proto import caffe_pb2 
import numpy as np 
import argparse 
from collections import defaultdict 

TRAIN_DATA_ROOT=''

if __name__ == "__main__":
    #caffe.set_mode_cpu()
    caffe.set_mode_gpu()
    parser = argparse.ArgumentParser()
    parser.add_argument('--proto', type=str, required=True)
    parser.add_argument('--model', type=str, required=True)
    parser.add_argument('--meanfile', type=str, required=True)
    parser.add_argument('--labelfile', type=str, required=True)
    parser.add_argument('--output', type=str, required=True)
    args = parser.parse_args()

    #proto_data = open(args.meanfile, 'rb').read()

    with open(args.meanfile,'rb') as infile:
            blob = caffe_pb2.BlobProto()
            blob.MergeFromString(infile.read())
            if blob.HasField('shape'):
                blob_dims = blob.shape
                assert len(blob_dims) == 4, 'Shape should have 4 dimensions - shape is "%s"' % blob.shape
            elif blob.HasField('num') and blob.HasField('channels') and blob.HasField('height') and blob.HasField('width'):
                blob_dims = (blob.num, blob.channels, blob.height, blob.width)
            else:
                raise ValueError('blob does not provide shape or 4d dimensions')
            mean = np.reshape(blob.data, blob_dims[1:]).mean(1).mean(1)

    #a = caffe.io.caffe_pb2.BlobProto.FromString(proto_data) 
    #mean = caffe.io.blobproto_to_array(a)[0] 
    #mean_file = caffe.io.blobproto_to_array(a)[0] 
    #mean=np.load(mean_file).mean(1).mean(1)

    net = caffe.Classifier(args.proto, args.model,
                       mean=mean,
                       channel_swap=(2,1,0),
                       raw_scale=255,
                       image_dims=(256, 256))
    count = 0
    correct = 0
    matrix = defaultdict(int) # (real,pred) -> int
    labels_set = set()

    #net = caffe.Net(args.proto, args.model, caffe.TEST)
    
    fwrite = open(args.output, 'w')
   
    f = open(args.labelfile, "r")
    for line in f.readlines():
  	parts = line.split()
        example_image = parts[0]
        #print("\rProcessing %i image (%s)" % (count,example_image))
	#fwrite.write("\rProcessing %i image (%s)" % (count,example_image))
        label = int(parts[1])
	input_image = caffe.io.load_image(TRAIN_DATA_ROOT + example_image)
        prediction = net.predict([input_image])
        #print("%.2f - %.2f" % (prediction[0][0], prediction[0][1]))
	#fwrite.write("%.2f - %.2f" % (prediction[0][0], prediction[0][1]))
        plabel = int(prediction[0].argmax())
        count += 1
        iscorrect = label == plabel
        correct += (1 if iscorrect else 0)
        matrix[(label, plabel)] += 1
        labels_set.update([label, plabel])
        if not iscorrect:
	    #fwrite.write("\rProcessing %i image (%s)" % (count,example_image))
	    fwrite.write("%s %.2f - %.2f" % (example_image,prediction[0][0], prediction[0][1]) + "\n" )
	    #fwrite.write("%.2f - %.2f" % (prediction[0][0], prediction[0][1]))
            #print("\rError: expected %i but predicted %i" % (label, plabel))
	    #fwrite.write("\rError: expected %i but predicted %i" % (label, plabel))
        #sys.stdout.write("\rAccuracy: %.1f%%" % (100.*correct/count))
	#fwrite.write("\rAccuracy: %.1f%%" % (100.*correct/count))
        sys.stdout.flush()
    print(", %i/%i corrects" % (correct, count))
    fwrite.write(", %i/%i corrects" % (correct, count) + "\n")
    print ""
    fwrite.write("\n")
    print "Confusion matrix:"
    fwrite.write("Confusion matrix: \n")
    print "(r , p) | count \n"
    fwrite.write("(r , p) | count \n")
    for l in labels_set:
        for pl in labels_set:
            print "(%i , %i) | %i" % (l, pl, matrix[(l,pl)])
	    fwrite.write("(%i , %i) | %i" % (l, pl, matrix[(l,pl)]) + "\n")
