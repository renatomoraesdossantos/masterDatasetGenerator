import numpy as np
import matplotlib.pyplot as plt
#%matplotlib inline

# Make sure that caffe is on the python path:
#caffe_root = '../'  # this file is expected to be in {caffe_root}/examples
import sys
#sys.path.insert(0, caffe_root + 'python')

import caffe

# Load the net, list its data and params, and filter an example image.
caffe.set_mode_gpu()

# Load the original network and extract the 'layers' parameters.
net = caffe.Net('A11/original/deploy.prototxt','A11/original/snapshot.caffemodel',caffe.TEST)
params = ['loss3/classifier']
fc_params = {pr: (net.params[pr][0].data, net.params[pr][1].data) for pr in params}
clone_params = {pr: (net.params[pr][0].data, net.params[pr][1].data) for pr in params}

#1.
net_conv_1 = caffe.Net('A11/1/deploy.prototxt','A11/1/snapshot.caffemodel',caffe.TEST)
conv_params_1 = {pr: (net_conv_1.params[pr][0].data, net_conv_1.params[pr][1].data) for pr in params}

#2.
net_conv_2 = caffe.Net('A11/2/deploy.prototxt','A11/2/snapshot.caffemodel',caffe.TEST)
conv_params_2 = {pr: (net_conv_2.params[pr][0].data, net_conv_2.params[pr][1].data) for pr in params}

#3.
net_conv_3 = caffe.Net('A11/3/deploy.prototxt','A11/3/snapshot.caffemodel',caffe.TEST)
conv_params_3 = {pr: (net_conv_3.params[pr][0].data, net_conv_3.params[pr][1].data) for pr in params}

#4.
net_conv_4 = caffe.Net('A11/4/deploy.prototxt','A11/4/snapshot.caffemodel',caffe.TEST)
conv_params_4 = {pr: (net_conv_4.params[pr][0].data, net_conv_4.params[pr][1].data) for pr in params}

#5.
net_conv_5 = caffe.Net('A11/5/deploy.prototxt','A11/5/snapshot.caffemodel',caffe.TEST)
conv_params_5 = {pr: (net_conv_5.params[pr][0].data, net_conv_5.params[pr][1].data) for pr in params}

#6.
net_conv_6 = caffe.Net('A11/6/deploy.prototxt','A11/6/snapshot.caffemodel',caffe.TEST)
conv_params_6 = {pr: (net_conv_6.params[pr][0].data, net_conv_6.params[pr][1].data) for pr in params}

#7.
net_conv_7 = caffe.Net('A11/7/deploy.prototxt','A11/7/snapshot.caffemodel',caffe.TEST)
conv_params_7 = {pr: (net_conv_7.params[pr][0].data, net_conv_7.params[pr][1].data) for pr in params}

print 'loop'

cnt = 8
#np.average() np.mean()

for pr in params:
    #conv_params[pr_conv][0].flat = fc_params[pr][0].flat  # flat unrolls the arrays
    #conv_params[pr_conv][1][...] = fc_params[pr][1]
    clone_params[pr][0][0] = fc_params[pr][0][0] + conv_params_1[pr][0][0] + conv_params_2[pr][0][0] + conv_params_3[pr][0][0] + conv_params_4[pr][0][0] + conv_params_5[pr][0][0] + conv_params_6[pr][0][0] + conv_params_7[pr][0][0] #Weights
    clone_params[pr][0][1] = fc_params[pr][0][1] + conv_params_1[pr][0][1] + conv_params_2[pr][0][1] + conv_params_3[pr][0][1] + conv_params_4[pr][0][1] + conv_params_5[pr][0][1] + conv_params_6[pr][0][1] + conv_params_7[pr][0][1] #Weights
    clone_params[pr][1][0] = fc_params[pr][1][0] + conv_params_1[pr][1][0] + conv_params_2[pr][1][0] + conv_params_3[pr][1][0] + conv_params_4[pr][1][0] + conv_params_5[pr][1][0] + conv_params_6[pr][1][0] + conv_params_7[pr][1][0] #Bias
    clone_params[pr][1][1] = fc_params[pr][1][1] + conv_params_1[pr][1][1] + conv_params_2[pr][1][1] + conv_params_3[pr][1][1] + conv_params_4[pr][1][1] + conv_params_5[pr][1][1] + conv_params_6[pr][1][1] + conv_params_7[pr][1][1] #Bias

    #cnt += 1

for pr in params:
    clone_params[pr][0][...] /= cnt
    clone_params[pr][1][...] /= cnt
    print clone_params[pr][0][...]
    print clone_params[pr][1][...]

print 'save'

net.save('A11.caffemodel')

