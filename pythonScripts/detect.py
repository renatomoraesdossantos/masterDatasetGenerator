import cv2
import os
import sys
import caffe
from caffe.proto import caffe_pb2
import numpy as np
import argparse
from collections import defaultdict

TRAIN_DATA_ROOT=''

if __name__ == "__main__":
    #caffe.set_mode_cpu()
    caffe.set_mode_gpu()
    parser = argparse.ArgumentParser()
    parser.add_argument('--proto', type=str, required=True)
    parser.add_argument('--model', type=str, required=True)
    parser.add_argument('--meanfile', type=str, required=True)
    parser.add_argument('--folder', type=str, required=True)
    parser.add_argument('--output', type=str, required=True)
    args = parser.parse_args()

    with open(args.meanfile,'rb') as infile:
	blob = caffe_pb2.BlobProto()
    	blob.MergeFromString(infile.read())
	if blob.HasField('shape'):
	    blob_dims = blob.shape
	    assert len(blob_dims) == 4, 'Shape should have 4 dimensions - shape is "%s"' % blob.shape
    	elif blob.HasField('num') and blob.HasField('channels') and blob.HasField('height') and blob.HasField('width'):
            blob_dims = (blob.num, blob.channels, blob.height, blob.width)
    	else:
            raise ValueError('blob does not provide shape or 4d dimensions')
        mean = np.reshape(blob.data, blob_dims[1:]).mean(1).mean(1)

    net = caffe.Classifier(args.proto, args.model,mean=mean,channel_swap=(2,1,0),raw_scale=255,image_dims=(256, 256))
    count = 0
    countNeg = 0
    countPos = 0
    correct = 0
    matrix = defaultdict(int) # (real,pred) -> int
    labels_set = set()

    N = 101
    offset = 50

    fwrite = open(args.output, 'w')

    for filename in os.listdir(args.folder):
    	img = cv2.imread(os.path.join(args.folder,filename))
	print (filename)
    
    	if img is not None:
    	    rows = img.shape[0]
	    cols = img.shape[1]
    	    for i in range(50,rows):
	    	for j in range(50,cols):
		    #Crop
		    y = i - offset;
    		    x = j - offset;
		    if ( N + x < cols and N + y < rows):
			print (("%i ; %i ; %i; %i") % (x,y,j,i))
		    	crop = img[x:y, N+j:N+i]
		    	crop = crop / 255.
                    	input_image = crop[:,:,(2,1,0)]
                    	prediction = net.predict([input_image])
        		
			fwrite.write("%s ; %i ; %i ; %.2f ; %.2f" % (filename, y, x, prediction[0][0], prediction[0][1]))
			fwrite.write("\n")

			#plabel = int(prediction[0].argmax())
        		plabel = int(prediction[0].argmax())
        		if plabel == 0:
            		    countNeg += 1
			    fwrite.write("%s ; %i ; %i; neg ; %.2f ; %.2f" % (filename, y, x, prediction[0][0], prediction[0][1]))
            		    #fwrite.write("%s ; %i ; neg ; %.2f ; %.2f" % (example_image, plabel, prediction[0][0], prediction[0][1]) + "\n" )
       			else:
            		    countPos += 1
			    print ("%s ; %i ; %i; %.2f ; %.2f" % (filename, y, x, prediction[0][0], prediction[0][1]))
			    fwrite.write("%s ; %i ; %i; pos ; %.2f ; %.2f" % (filename, y, x, prediction[0][0], prediction[0][1]))
            		    #fwrite.write("%s ; %i ; pos ; %.2f ; %.2f" % (example_image, plabel, prediction[0][0], prediction[0][1]) + "\n" )
        		count += 1

    fwrite.write("Negativos : ")
    fwrite.write("%i" % (countNeg) + "\n")
    fwrite.write("Positivos : ")
    fwrite.write("%i" % (countPos) + "\n")

