#include <stdio.h>
#include <iostream>
#include <QDir>
#include <QDirIterator>
#include <QDebug>
#include <QCoreApplication>
#include <QFile>
#include <QStringList>
#include <QTimer>
#include <QTime>
#include <vector>
#include <algorithm>
#include <random>

using namespace std;

int main(int argc, char** argv) {
    QCoreApplication app(argc, argv);
    QTime* timer = new QTime();
    timer->start();

    QDir firstDir("/data/renatosantos/datasets/2/1x1/train_gpu_TPS/pos/");
    QDir secondDir("/data/renatosantos/datasets/2/1x1/train_gpu_TPS/neg/");

    //QDir firstDir("/data/renatosantos/datasets/mitos_notps/Training/pos/orig/");
    //QDir secondDir("/data/renatosantos/datasets/mitos_notps/Training/neg/orig/");

    int firstCount = firstDir.count();
    int secondCount = secondDir.count();
    qDebug() << firstCount;
    qDebug() << secondCount;
    int diffDirs = 0;

    auto engine = std::default_random_engine{};

    if (firstCount > secondCount)
    {
        diffDirs = firstCount - secondCount;
        QStringList filters;
        filters << "*.png" << "*.jpg" << "*.bmp" << "*.tiff" << "*.gif";
        firstDir.setFilter(QDir::Files|QDir::NoDotAndDotDot);
        firstDir.setNameFilters(filters);
        QFileInfoList list = firstDir.entryInfoList(filters, QDir::Files|QDir::NoDotAndDotDot);

        shuffle(begin(list), end(list), engine);

        for (int i = diffDirs; i--;)
        {
	    qDebug() << list.at(i).fileName();
            firstDir.remove(list.at(i).fileName());
            if (firstDir.count() == secondDir.count())
                break;
        }
    }
    else if(firstCount < secondCount)
    {
        diffDirs = secondCount - firstCount;
        QStringList filters;
        filters << "*.png" << "*.jpg" << "*.bmp" << "*.tiff" << "*.gif" << "*.csv";
        secondDir.setFilter(QDir::Files|QDir::NoDotAndDotDot);
        secondDir.setNameFilters(filters);
        QFileInfoList list = secondDir.entryInfoList(filters, QDir::Files|QDir::NoDotAndDotDot);

	shuffle(begin(list), end(list), engine);

        for (int i = diffDirs; i--;)
        {
	    qDebug() << list.at(i).fileName();
            secondDir.remove(list.at(i).fileName());
            if (firstDir.count() == secondDir.count())
                break;
        }
    }
    else
    {
        //Do nothing.
    }
    int nMilliseconds = timer->elapsed();
    qDebug() << "Tempo total de processamento: " << nMilliseconds << "milisegundos";
    return 0;
}
