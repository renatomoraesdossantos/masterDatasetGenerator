#DatasetGenerator
This project was developed for data augmentation of these datasets: MITOS (ICPR 2012) and MITOS ATYPIA (ICPR 2014).
To execute the augmenting, edit the DatasetGenerator.sh text file and execute the following command:
$./DatasetGenerator.sh

#Balance
This project is a script for balancing the dataset between positives and negatives.
To execute this script edit the main.cpp and compile again with make.

#DeleteLines
This project is a script for deleting lines with a specific word, for our labels text files.
To execute this script edit the main.cpp and compile again with make.

#SplitImages
This project is a script for split the original images from the dataset for test and generates labels text files for each image.
To execute this script edit the main.cpp and compile again with make.

#Divide
This project is a script to divide the images from the dataset in the following directories: train/ val/ test/ for we use in our model training.
To execute this script edit the main.cpp and compile again with make.

#pythonScripts
This project contains python scripts for post-processing the model for detection, combine networks and test.
Some samples how to execute the scripts:
$python test.py --proto deploy.prototxt --model snapshot.caffemodel --meanfile mean.binaryproto --labelfile test.txt

$python proc.py --proto deploy.prototxt --model snapshot.caffemodel --meanfile mean.binaryproto --labelfile ../A06.txt --output A06_log.txt

$python pred.py --proto deploy.prototxt --model snapshot.caffemodel --meanfile mean.binaryproto --labelfile ../A03.txt --output A03_log.txt && python pred.py --proto deploy.prototxt --model snapshot.caffemodel --meanfile mean.binaryproto --labelfile ../A04.txt --output A04_log.txt && python pred.py --proto deploy.prototxt --model snapshot.caffemodel --meanfile mean.binaryproto --labelfile ../A05.txt --output A05_log.txt && python pred.py --proto deploy.prototxt --model snapshot.caffemodel --meanfile mean.binaryproto --labelfile ../A07.txt --output A07_log.txt && python pred.py --proto deploy.prototxt --model snapshot.caffemodel --meanfile mean.binaryproto --labelfile ../A10.txt --output A10_log.txt && python pred.py --proto deploy.prototxt --model snapshot.caffemodel --meanfile mean.binaryproto --labelfile ../A11.txt --output A11_log.txt && python pred.py --proto deploy.prototxt --model snapshot.caffemodel --meanfile mean.binaryproto --labelfile ../A12.txt --output A12_log.txt && python pred.py --proto deploy.prototxt --model snapshot.caffemodel --meanfile mean.binaryproto --labelfile ../A14.txt --output A14_log.txt && python pred.py --proto deploy.prototxt --model snapshot.caffemodel --meanfile mean.binaryproto --labelfile ../A15.txt --output A15_log.txt && python pred.py --proto deploy.prototxt --model snapshot.caffemodel --meanfile mean.binaryproto --labelfile ../A17.txt --output A17_log.txt && python pred.py --proto deploy.prototxt --model snapshot.caffemodel --meanfile mean.binaryproto --labelfile ../A18.txt --output A18_log.txt

$python pred.py --proto deploy.prototxt --model snapshot.caffemodel --meanfile mean.binaryproto --labelfile ../A06.txt --output A06_log.txt && python pred.py --proto deploy.prototxt --model snapshot.caffemodel --meanfile mean.binaryproto --labelfile ../A08.txt --output A08_log.txt && python pred.py --proto deploy.prototxt --model snapshot.caffemodel --meanfile mean.binaryproto --labelfile ../A09.txt --output A09_log.txt && python pred.py --proto deploy.prototxt --model snapshot.caffemodel --meanfile mean.binaryproto --labelfile ../A13.txt --output A13_log.txt && python pred.py --proto deploy.prototxt --model snapshot.caffemodel --meanfile mean.binaryproto --labelfile ../A16.txt --output A16_log.txt

$python detect.py --proto deploy.prototxt --model snapshot.caffemodel --meanfile mean.binaryproto --folder /home/renatosantos/MITOS-APERIO-x40/orig/test/Scanner_Aperio/A06/frames/x40/ --output A06.txt

$python join.py
