TEMPLATE = app
#CONFIG += console

QMAKE_CXXFLAGS += -std=c++11

CONFIG -= app_bundle
CONFIG += qt

QT += core gui

SOURCES += main.cpp
