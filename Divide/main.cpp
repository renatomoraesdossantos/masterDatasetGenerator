//#include <stdio.h>
#include <iostream>
#include <QDir>
#include <QDirIterator>
#include <QDebug>
#include <QCoreApplication>
#include <QFile>
#include <QStringList>
#include <QTimer>
#include <QTime>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <math.h>
//#include <random>

using namespace std;


///CSVs Sufixes
const static QString positive = "_mitosis";
const static QString negative = "_not_mitosis";
const static QString csv = ".csv";

inline QString withoutExtension(const QString & fileName) {
    return fileName.left(fileName.lastIndexOf("."));
}

inline QString onlyExtension(const QString & fileName) {
    return fileName.mid(fileName.lastIndexOf("."),fileName.length());
}

int main(int argc, char** argv) {
    QCoreApplication app(argc, argv);
    QTime* timer = new QTime();
    timer->start();

    vector<QFileInfo> train;
    vector<QFileInfo> val;
    vector<QFileInfo> test;

    std::random_shuffle(train.begin(),train.end());

    ////Paths
    const static QString sourcePath = "/data/renatosantos/datasets/Aperio_Base/";
    const static QString trainPath = "/data/renatosantos/datasets/1/train_/";
    const static QString testPath = "/data/renatosantos/datasets/1/test_/";
    const static QString valPath = "/data/renatosantos/datasets/1/val_/";

//    QDir trainDir("/data/renatosantos/datasets/1/train/");
//    QDir testDir("/data/renatosantos/datasets/1/test/");
//    QDir valDir("/data/renatosantos/datasets/1/val/");

    QDir directory(sourcePath);
    if (!directory.exists()) {
       cout << "The directory "+ sourcePath.toStdString() + " not exist.";
       return 0;
    }

    QStringList filters;
    filters << "*.png" << "*.jpg" << "*.bmp" << "*.tiff" << "*.gif";

    directory.setFilter(QDir::Files|QDir::NoDotAndDotDot);
    directory.setNameFilters(filters);

    QDirIterator directories(directory, QDirIterator::Subdirectories);


    while (directories.hasNext())
    {
        directories.next();
        QFileInfo fileInfo = directories.fileInfo();

        train.push_back(fileInfo);
    }


    //Shuffle
    random_shuffle(train.begin(),train.end());
    std::vector<QFileInfo> lines = train;

    // fill
    std::size_t const new_size = ceilf(train.size()*0.25);
    //std::vector<QFileInfo> splitTest(lines.begin(), lines.begin() + new_size);
    //std::vector<QFileInfo> splitTrain(lines.begin() + new_size, lines.end());

    //qDebug() << splitTest.size();
    //qDebug() << splitTest[1].filePath();
    //test = splitTest;
    //train = splitTrain;

    //Shuffle
    //random_shuffle(train.begin(),train.end());
    //lines = train;
    // fill
    std::vector<QFileInfo> splitVal(lines.begin(), lines.begin() + new_size);
    std::vector<QFileInfo> splitNewTrain(lines.begin() + new_size, lines.end());

    val = splitVal;
    train = splitNewTrain;

    QString filename = "script.sh";
    QFile file(filename);
    if (file.open(QIODevice::ReadWrite)) {
        QTextStream stream(&file);
        stream << "#/bin/bash" << endl;
   // }


    ///TEST

    /*for (int i=0; i< test.size(); i++)
    {
        qDebug() << testPath + test[i].absoluteFilePath();

        QFileInfo name = test[i];

	stream << "cp " <<  name.absoluteFilePath() << " " << testPath << endl;
	stream << "cp " <<  name.path() << "/" << name.baseName() << positive << csv << " " << testPath << endl;
	stream << "cp " <<  name.path() << "/" << name.baseName() << negative << csv << " " << testPath << endl;

        QFile::copy(name.absoluteFilePath(), testPath + name.fileName());
        QFile::copy(name.path() + "/" + name.baseName() + negative + csv, testPath + name.baseName() + negative + csv);
        QFile::copy(name.path() + "/" + name.baseName() + positive + csv, testPath + name.baseName() + positive + csv);
    }*/

    //stream << endl;

    ///VALIDATION

    for (int i=0; i< val.size(); i++)
    {
      	qDebug() << val[i].absoluteFilePath();

        QFileInfo name = val[i];
	stream << "cp " <<  name.absoluteFilePath() << " " << valPath << endl;
	stream << "cp " <<  name.path() << "/" << name.baseName() << positive << csv << " " << valPath << endl;
	stream << "cp " <<  name.path() << "/" << name.baseName() << negative << csv << " " << valPath << endl;

        QFile::copy(name.absoluteFilePath(), valPath + name.fileName());
        QFile::copy(name.path() + "/" + name.baseName() + negative + csv, valPath + name.baseName() + negative + csv);
        QFile::copy(name.path() + "/" + name.baseName() + positive + csv, valPath + name.baseName() + positive + csv);
    }

    //stream << endl;

    ///TRAINING

    for (int i=0; i< train.size(); i++)
    {
        qDebug() << train[i].absoluteFilePath();

        QFileInfo name = train[i];
	stream << "cp " <<  name.absoluteFilePath() << " " << trainPath << endl;
	stream << "cp " <<  name.path() << "/" << name.baseName() << positive << csv << " " << trainPath << endl;
	stream << "cp " <<  name.path() << "/" << name.baseName() << negative << csv << " " << trainPath << endl;

        QFile::copy(name.absoluteFilePath(), trainPath + name.fileName());
        QFile::copy(name.path() + "/" + name.baseName() + negative + csv, trainPath + name.baseName() + negative + csv);
        QFile::copy(name.path() + "/" + name.baseName() + positive + csv, trainPath + name.baseName() + positive + csv);
    }

    }

    file.close();

    return 0;
}

