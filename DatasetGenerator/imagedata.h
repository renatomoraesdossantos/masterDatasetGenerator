#ifndef IMAGEDATA_H
#define IMAGEDATA_H

#include <QObject>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"


struct mitosis {
    double x;
    double y;
    double grade;
};

using namespace std;
using namespace cv;

/**
 * @brief The ImageData class
 */
class ImageData
{

    public:
        /**
         * @brief ImageData
         */
        ImageData();

        /**
         *
         */
        virtual ~ImageData();

        /**
         * @brief flipImage
         * @param originalImage
         * @param flipCode
         * @return
         */
        Mat flipImage(Mat originalImage, int flipCode);

        /**
         * @brief rotateImage
         * @param originalImage
         * @param angle
         * @return
         */
        Mat rotateImage(Mat originalImage, double angle);

        /**
         * @brief cropImage
         * @param original
         * @param coordinate
         * @param rectSize
         * @param offset
         * @return
         */
        Mat cropImage(Mat original, Point coordinate, Size rectSize, int offset);

        /**
         * @brief generateGrid
         * @param originalImage
         * @param cols
         * @param rows
         */
        vector<Point2f> generateGrid(Mat originalImage, int cols, int rows);


    private:
        Point coordinate;
        Size rectSize;
        int offset;

};

#endif // IMAGEDATA_H
