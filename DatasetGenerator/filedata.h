#ifndef FILEDATA_H
#define FILEDATA_H

#include <QObject>
#include <QDir>
#include <QCoreApplication>
#include <QFile>
#include <QStringList>
#include <QTextStream>
#include "imagedata.h"
#include <vector>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;
using namespace std;

/**
 * @brief The FileData class
 */
class FileData : public ImageData
{

    public:
        /**
         * @brief FileData
         */
        FileData();

        /**
         * @brief withoutExtension
         * @param fileName
         * @return
         */
        inline QString withoutExtension(const QString & fileName) {
            return fileName.left(fileName.lastIndexOf("."));
        }

        /**
         * @brief onlyExtension
         * @param fileName
         * @return
         */
        inline QString onlyExtension(const QString & fileName) {
            return fileName.mid(fileName.lastIndexOf("."),fileName.length());
        }

        /**
         * @brief csvReaderPoints
         * @param fileName
         * @param separator
         * @return
         */
        vector<Point> csvReaderPoints(QString fileName, const QString separator);

        /**
         * @brief csvReaderMitosis
         * @param fileName
         * @param separator
         * @return
         */
        vector<mitosis> csvReaderMitosis(QString fileName, const QString separator);

        /**
         * @brief csvReadervectorPoints
         * @param fileName
         * @param separator
         * @return
         */
        vector<vector<Point> > csvReadervectorPoints(QString fileName, const QString separator);

    private:
        QString fileExtension;
        QString fileName;


};

#endif // FILEDATA_H
