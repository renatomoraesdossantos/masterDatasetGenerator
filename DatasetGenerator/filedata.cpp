#include "filedata.h"
#include <stdio.h>
#include <iostream>

FileData::FileData()
{

}

vector<Point> FileData::csvReaderPoints(QString fileName, const QString separator)
{
    vector<Point> pointvector;
    pointvector.clear();

    QFile file(fileName);
    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream textStream(&file);

        while( !textStream.atEnd() )
        {
            QString line = textStream.readLine();
            QStringList values = line.split(separator);

            double x = values[0].toDouble();
            double y = values[1].toDouble();

            Point imagePoint(x,y);
            pointvector.push_back(imagePoint);

        }
    }

    return pointvector;

}

vector<mitosis> FileData::csvReaderMitosis(QString fileName, const QString separator)
{
    vector<mitosis> pointvector;
    pointvector.clear();

    mitosis mitosisCoordinates;

    QFile file(fileName);
    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream textStream(&file);

        while( !textStream.atEnd() )
        {
            QString line = textStream.readLine();
            QStringList values = line.split(separator);

            mitosisCoordinates.x = values[0].toDouble();
            mitosisCoordinates.y = values[1].toDouble();
            mitosisCoordinates.grade = values[2].toDouble();

            pointvector.push_back(mitosisCoordinates);

        }
    }

    return pointvector;

}

vector<vector<Point> > FileData::csvReadervectorPoints(QString fileName, const QString separator)
{
    vector<vector<Point> > vectorPointvector;
    vector<Point> pointvector;
    vectorPointvector.clear();
    pointvector.clear();

    QFile file(fileName);
    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream textStream(&file);

        while( !textStream.atEnd() )
        {
            QString line = textStream.readLine();
            QStringList values = line.split(separator);

            int vectorSize = values.size()/2;
            int vectorPairControl = 0;

            for (int i=0; i < values.size(); i++)
            {
                if (vectorPairControl < vectorSize) {

                    double x = values[i].toDouble();
                    i++;
                    double y = values[i].toDouble();

                    Point imagePoint(x,y);
                    pointvector.push_back(imagePoint);

                    vectorPairControl++;

                } else {

                    continue;

                }

            }

            vectorPointvector.push_back(pointvector);

        }
    }

    return vectorPointvector;

}
