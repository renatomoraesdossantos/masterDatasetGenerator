#ifndef _FILE_CONSTANTS_

#define	N_SAMPLES_1D	10

#ifndef PI
#define PI 3.14159265
#endif

#ifndef PIover180
#define PIover180 3.14159265/180
#endif

#define C_SCALE_MIN 0.9
#define C_SCALE_MAX 1.1

//in grades:
#define C_ROT_MIN	-15
#define C_ROT_MAX	15

#define C_TRANS_MIN	-10
#define C_TRANS_MAX  10

#define C_NLAYERS	3

#endif
