#include "imagedata.h"

ImageData::ImageData()
{

}

ImageData::~ImageData()
{

}

Mat ImageData::rotateImage(Mat originalImage, double angle)
{
    Mat rotatedImage;
    Point2f pt(originalImage.cols/2., originalImage.rows/2.);
    Mat r = getRotationMatrix2D(pt, angle, 1.0);
    warpAffine(originalImage, rotatedImage, r, Size(originalImage.cols, originalImage.rows));
    return rotatedImage;
}


Mat ImageData::flipImage(Mat originalImage, int flipCode)
{
    cv::Mat flippedImage;
    cv::flip(originalImage,flippedImage,flipCode);
    return flippedImage;
}

Mat ImageData::cropImage(Mat originalImage, Point coordinate, Size rectSize, int offset)
{
    cv::Mat tempImage(originalImage);
    cv::Rect rect = cv::Rect(coordinate.x - offset,coordinate.y - offset, rectSize.width, rectSize.height);
    cv::Mat croppedImage = tempImage(rect);
    return croppedImage;
}

vector<Point2f> ImageData::generateGrid(Mat originalImage, int cols, int rows)
{
    vector<Point2f> pointVector;
    int colspacing = originalImage.cols / cols;
    int rowspacing = originalImage.rows / rows;


    Point2f tmpPoint = Point2f(0, 0);

    for (int i = 0; i < cols; ++i )
    {
        tmpPoint.x + colspacing;
        for (int j = 0; j < rows; ++j)
        {
            tmpPoint.y + rowspacing;
            pointVector.push_back(tmpPoint);
        }
    }

    return pointVector;
}

