//#define _GLIBCXX_USE_CXX11_ABI 0

#include <stdio.h>
#include <iostream>
#include <QDir>
#include <QDirIterator>
#include <QDebug>
#include <QCoreApplication>
#include <QFile>
#include <QStringList>
#include <opencv2/opencv.hpp>
#include <QTimer>
#include <QTime>
#include <vector>
#include "ThinPlateSpline/ThinPlateSpline.h"
#include "log.h"
#include <cstdlib>

//#include <algorithm>
//#include <random>

//GPU Include
#include "opencv2/gpu/gpu.hpp"

using namespace cv;
using namespace std;

/**
 * @brief The mitosis struct
 */
struct mitosis {
    double x;
    double y;
    double grade;
};

/**
 * @brief Images size
 */
struct images {
    double cols;
    double rows;
};

/**
 * @brief The mitosisFile struct
 */
struct mitosisFile {
    QString fileAbsolutePath;
    QString fileName;
    QString fileExtension;
    QString csvPositive;
    QString csvNegative;
    vector<mitosis> mitoses;
    vector<mitosis> notMitoses;
    //Mat imageRead;
};

static const int gridWindow = 10;
static const int window = 50;
static const int gridSize = 21;
static const int windowSize = 101;

static int testGpu = 0;

const static int MITOS = 2012;
const static int ATYPIA = 2014;

///Paths
const static QString positivesPath = "pos/";
const static QString negativesPath = "neg/";
const static QString rotateImagesPath = "rot/";
const static QString originalPath = "orig/";
const static QString thinPlateSplinesImagesPath = "tps/";

///CSVs Sufixes
const static QString positive = "_mitosis";
const static QString negative = "_not_mitosis";
const static QString csv = ".csv";

void flipImage(Mat &src, Mat &dst, int flipCode);
void rotateImage(Mat &src, Mat &dst, double angle);
bool cropImage(Mat &src, Mat &croppedImage, Point coordinate, Size rectSize, int offset);
void cropRect(images src, Point coordinate, Size rectSize, int offset, vector<mitosis> &mitos);
vector<mitosis> expandMitosis(vector<mitosis> mitoses, images imageBase, Size size);
vector<Point> csvReaderPoints(QString fileName, const QString separator);
vector<mitosis> csvReaderMitosis(QString fileName, const QString separator);
vector<Point> csvReaderVectorPoints(QString fileName, const QString separator);
vector<mitosis> csvReaderVectorMitosis(QString fileName, const QString separator);
vector<Point> createGrid(Mat &originalImage, int inc);
vector<Point> generateGrid(Mat &originalImage, int cols, int rows, float inc);
vector<Point> generateMitosisGrid(Mat &originalImage, vector<mitosis> mitosis, float inc);
vector<Point> changeGrid(vector<Point> pointVector, vector<mitosis> mitosis);
double calculateDistance(const Point& pt1, const Point& pt2);
int writeMitosisFiles(vector<mitosis> mitoses, Mat &imageBase, QString destinyPath, QString fileName, Size size);
void cropROI(vector<Point> points, Mat &image, Mat &crop);
void centerCropROI(vector<Point> points, Point &roiCenter);

inline QString withoutExtension(const QString & fileName) {
    return fileName.left(fileName.lastIndexOf("."));
}

inline QString onlyExtension(const QString & fileName) {
    return fileName.mid(fileName.lastIndexOf("."),fileName.length());
}

static void help()
{
    printf("\nThis program augmenting images datasets.\n"
            "Using image processing:\n"
            "\n"
            "Usage:\n DatasetGenerator <input dir> <output dir> <TPS (true or false) <grid (2 - 2x2, 4 - 4x4, ...)> <MITOS(2012) or ATYPIA(2014)>\n");
    exit(0);
}

int main(int argc, char** argv)
{

    QCoreApplication app(argc, argv);
    
    //LogImgDataset::getInstance().Log(INFO,"Main()");

    QTime* timer = new QTime();
    timer->start();

    if (argc < 5)
	help();


    try
    {

        ////Paths
	const static QString sourcePath = QString(argv[1]);
	const static QString destinationPath = QString(argv[2]);
    	const static QString sTPS = QString(argv[3]);
    	const static int grid = atoi(argv[4]);
	static int dataset = atoi(argv[5]);

	///TPS
    	static bool TPS = false;
    	if (sTPS.toLower() == "false")
            TPS = false;
    	else if (sTPS.toLower() == "true")
            TPS = true;
    	else
            TPS = false;

	///MITOS OR ATYPIA
	if (atoi(argv[5]) == MITOS)
	    dataset = MITOS;
	else
	    dataset = ATYPIA;

        //const static QString sourcePath = "/data/renatosantos/datasets/1/train/";//argv[1];
	//const static QString sourcePath = "/home/renatosantos/Input/";
	//const static QString sourcePath = "/home/renatosantos/MITOS/TrainingDataSet/ScannerA/";
	//const static QString sourcePath = "/home/renatosantos/MITOS/TestingDataSet/mitosis_evaluation_set_A/";
        //const static QString destinationPath = "/data/renatosantos/datasets/2/tps_wm/";
	//const static QString destinationPath = "/home/renatosantos/Output/";

	//static bool TPS = false;
        //static bool TPS = true;
	
        //static int grid = 2; //atoi( argv[3] );

	testGpu = gpu::getCudaEnabledDeviceCount();
	qDebug() << "N GPUs = "<< testGpu;

        QDir directory(sourcePath);
        if (!directory.exists()) {
           cout << "The directory "+ sourcePath.toStdString() + " not exist.";
           return 0;
        }

        ///Root destination directory
        QDir destinationDirectory(destinationPath);
        destinationDirectory.mkdir(destinationPath);
        destinationDirectory.mkdir(positivesPath);
        destinationDirectory.mkdir(negativesPath);

        ///Positives Tree
        QString posDir = destinationPath + positivesPath;
        QDir posDirectory(posDir);

        ///Negatives Tree
        QString negDir = destinationPath + negativesPath;
        QDir negDirectory(negDir);

        ///TPS Trees
        QString posTPSDir;
        QString negTPSDir;

        QStringList filters;
        filters << "*.png" << "*.jpg" << "*.bmp" << "*.tiff" << "*.gif";

        directory.setFilter(QDir::Files|QDir::NoDotAndDotDot);
        directory.setNameFilters(filters);

        int mitosisPositivesTotal = 0;
        int mitosisNegativesTotal = 0;
        int mitosisAltPositivesTotal = 0;
        int mitosisAltNegativesTotal = 0;

        QVector<mitosisFile> mitosisVector;

        QDirIterator directories(directory, QDirIterator::Subdirectories);

        QTime* timerReadingDir = new QTime();
        timerReadingDir->start();

        while (directories.hasNext())
        {

            mitosisFile mitosFile;
            mitosFile.fileAbsolutePath = directories.next();
            mitosFile.fileName = directories.fileInfo().baseName();
            mitosFile.fileExtension = onlyExtension(directories.fileName());

	    vector<mitosis> posMitosis;
	    vector<mitosis> negMitosis;

	    if (dataset == ATYPIA)
	    {
            	mitosFile.csvPositive = withoutExtension(mitosFile.fileAbsolutePath) + positive + csv;
            	mitosFile.csvNegative = withoutExtension(mitosFile.fileAbsolutePath) + negative + csv;
		posMitosis = csvReaderVectorMitosis(mitosFile.csvPositive, ",");
		negMitosis = csvReaderVectorMitosis(mitosFile.csvNegative, ",");
	    }
	    else
	    {
		mitosFile.csvPositive = withoutExtension(mitosFile.fileAbsolutePath) + csv;
		mitosFile.csvNegative = withoutExtension(mitosFile.fileAbsolutePath) + negative + csv;
		posMitosis = csvReaderMitosis(mitosFile.csvPositive, ",");
		negMitosis = csvReaderMitosis(mitosFile.csvNegative, ",");
	    }

	    //vector<mitosis> posMitosis = csvReaderVectorMitosis(mitosFile.csvPositive, ",");
            //vector<mitosis> posMitosis = csvReaderMitosis(mitosFile.csvPositive, ",");
            mitosFile.mitoses = posMitosis;
            mitosisPositivesTotal += posMitosis.size();

	    //vector<mitosis> negMitosis = csvReaderVectorMitosis(mitosFile.csvNegative, ",");
            //vector<mitosis> negMitosis = csvReaderMitosis(mitosFile.csvNegative, ",");
            mitosFile.notMitoses = negMitosis;
            mitosisNegativesTotal += negMitosis.size();

            /*
	    Mat imagetmp = cv::imread(mitosFile.fileAbsolutePath.toStdString());
            images imageRead;
            imageRead.cols = imagetmp.cols;
            imageRead.rows = imagetmp.rows;
            imagetmp.release();
	    */

            //vector<mitosis> altMitoses = expandMitosis(posMitosis, imageRead, Size(gridSize,gridSize));
            //mitosFile.mitoses = altMitoses;
            //vector<mitosis> altNotMitoses = expandMitosis(negMitosis, imageRead, Size(gridSize,gridSize));
            //mitosFile.notMitoses = altNotMitoses;
            //mitosisAltPositivesTotal += altMitoses.size();
            //mitosisAltNegativesTotal += altNotMitoses.size();

            mitosisVector.push_back(mitosFile);

        }
        qDebug() << "Tempo Leitura Diretorios: " << timerReadingDir->elapsed() << "milisegundos";

        //auto engine = std::default_random_engine{};
        //std:shuffle(std::begin(mitosisVector), std::end(mitosisVector), engine);

        qDebug() << "Positives: " << mitosisPositivesTotal;
        qDebug() << "Negatives: " << mitosisNegativesTotal;

        //qDebug() << "ALT Positives: " << mitosisAltPositivesTotal;
        //qDebug() << "ALT Negatives: " << mitosisAltNegativesTotal;

        int negativesMitosisCounter = 0;
        int positivesMitosisCounter = 0;

        //qDebug() << "Positivos";

        ///Train Positives
        for (int i = 0; i < mitosisVector.size(); i++)
        {
            QString fileName = mitosisVector.at(i).fileName;
            qDebug() << fileName;

            //Mat imageBase = mitosisVector.at(i).imageRead;
            Mat imageBase = cv::imread(mitosisVector.at(i).fileAbsolutePath.toStdString());

            ///Leitura dos arquivos CSV
            QString csvFilenamePositive = mitosisVector.at(i).csvPositive;
            vector<mitosis> mitoses = mitosisVector.at(i).mitoses;
            QString csvFilenameNegative = mitosisVector.at(i).csvNegative;
            vector<mitosis> notMitoses = mitosisVector.at(i).notMitoses;

	    if (TPS == false)
	    {
	   	///Recorte e Rotações
		int pos = writeMitosisFiles(mitoses, imageBase, posDir, fileName, Size(windowSize,windowSize));
		positivesMitosisCounter = positivesMitosisCounter + pos;
	    }

            else
            {

                std::vector<cv::Point> iP, iiP;

                Mat imageCopy = imageBase.clone();
		vector<mitosis> posMitosis = mitoses;

                //Incremento randomico de 1 a 30
                int inc = rand() % 30 + 1;
		//int inc = 10;
                //Grid
                int cols = grid;
                int rows = grid;
		
		// push some points into the vector for the source image
		/*iP.push_back(cv::Point(150,150));
		iP.push_back(cv::Point(1200,150));
		iP.push_back(cv::Point(150,1200));
		iP.push_back(cv::Point(1200,1200));
		iP.push_back(cv::Point(768,768));
		iP.push_back(cv::Point(450,256));

		// push some point into the vector for the dst image
		iiP.push_back(cv::Point(210,210));
		iiP.push_back(cv::Point(1290,180));
		iiP.push_back(cv::Point(180,1230));
		iiP.push_back(cv::Point(1290,1260));
		iiP.push_back(cv::Point(660,840));
		iiP.push_back(cv::Point(540,720));*/

		//Grid Normal 2x2 3x3 4x4 5x5 ...
                iP = generateGrid(imageCopy,rows,cols, 0);
                iiP = generateGrid(imageCopy,rows,cols, inc);
		//iP = createGrid(imageCopy, 0);
		//iiP = createGrid(imageCopy, inc);
		
		//Deslocando apenas as mitoses e mantendo as bordas fixas
		//iP = generateMitosisGrid(imageCopy, posMitosis, 0);
		//iiP = generateMitosisGrid(imageCopy, posMitosis, inc);

		bool tpsOk = false;
		//qDebug() << iP.size() << iiP.size();
		if (iP.size() > 0 && iiP.size() > 0 && iP.size() == iiP.size())
		{
		    tpsOk = true;
		    //qDebug() << "OK";
		}

                //vector<mitosis> negMitosis = notMitoses;

                vector<Point> pSource;
                pSource.clear();
                for(int i = posMitosis.size(); i--;)
                {
                    Point pointSource(posMitosis[i].x,posMitosis[i].y);
		    Point pointNew(posMitosis[i].x+inc, posMitosis[i].y+inc);
                    pSource.push_back(pointSource);
		    iP.push_back(pointSource);
		    iiP.push_back(pointNew);
                }

                vector<Point> nSource;
                nSource.clear();
                //for(int i = negMitosis.size(); i--;)
                //{
                //    Point pointSource(negMitosis[i].x,negMitosis[i].y);
                //    nSource.push_back(pointSource);
                //}

                Mat thinPlateSplineImage;
                //bool tpsOk = thinPlateSplineProcessing(imageCopy, thinPlateSplineImage, iP, iiP, pSource, nSource);

                if (tpsOk)
		{
			thinPlateSplineProcessing(imageCopy, thinPlateSplineImage, iP, iiP, pSource, nSource);
			//cv::imwrite((destinationPath + fileName + "_TPS.jpg").toStdString(), thinPlateSplineImage);

			if ((int) pSource.size() > 0)
        	        {
                	    posMitosis.clear();
	                    for(int i = pSource.size(); i--;)
	                    {
	                        mitosis mitosisCoord;
	                        mitosisCoord.x = pSource[i].x;
	                        mitosisCoord.y = pSource[i].y;
	                        mitosisCoord.grade = 0;
	                        posMitosis.push_back(mitosisCoord);
	                    }

        	            int posTPS = writeMitosisFiles(posMitosis, thinPlateSplineImage, posDir, fileName, Size(windowSize,windowSize));
			    positivesMitosisCounter = positivesMitosisCounter + posTPS;
			}
            	    }

            	}
	    }

            ///Train Negatives
		   
	   /*for (int j = 0; j < mitosisVector.size(); j++)
           // while (mitosisPositivesTotal > negativesMitosisCounter && j < mitosisVector.size())
           {

                QString fileName = mitosisVector.at(j).fileName;
                qDebug() << fileName;

                Mat imageBase = cv::imread(mitosisVector.at(j).fileAbsolutePath.toStdString());

                ///Leitura dos arquivos CSV
                QString csvFilenameNegative = mitosisVector.at(j).csvNegative;
                vector<mitosis> notMitoses = mitosisVector.at(j).notMitoses;
                //qDebug() << notMitoses.size();

                ///Expandindo os pontos para gerar novas imagens
                //notMitoses = expandMitosis(notMitoses, imageBase, Size(gridSize,gridSize));

		if (TPS == false)
		{
		   int neg = writeMitosisFiles(notMitoses, imageBase, negDir, fileName, Size(windowSize,windowSize));
		   negativesMitosisCounter = negativesMitosisCounter + neg;
		}
                else
                {
                    std::vector<cv::Point> iP, iiP;

                    Mat imageCopy = imageBase.clone();
		    vector<mitosis> negMitosis = notMitoses;

                    //Incremento randomico de 1 a 30
                    int inc = rand() % 30 + 1;
		    //int inc = 10;
                    //Grid
                    int cols = grid;
                    int rows = grid;

		    //Grid Normal 2x2 3x3 4x4 5x5 ...
		    iP = generateGrid(imageCopy,rows,cols, 0);
                    iiP = generateGrid(imageCopy,rows,cols, inc);
                    //iP = createGrid(imageCopy, 0);
                    //iiP = createGrid(imageCopy, inc);

                    //Deslocando apenas as mitoses e mantendo as bordas fixas
                    //iP = generateMitosisGrid(imageCopy, negMitosis, 0);
                    //iiP = generateMitosisGrid(imageCopy, negMitosis, inc);

		    bool tpsOk = false;
		    if (iP.size() > 0 && iiP.size() > 0 && iP.size() == iiP.size())
		    {
			tpsOk = true;
			//qDebug()<<"ok!";
		    }

                    //vector<mitosis> posMitosis = mitoses;

                    vector<Point> pSource;
                    pSource.clear();
                    //for(int i = posMitosis.size(); i--;)
                    //{
                    //    Point pointSource(posMitosis[i].x,posMitosis[i].y);
                    //    pSource.push_back(pointSource);
                    //}

                    vector<Point> nSource;
                    nSource.clear();
                    for(int i = negMitosis.size(); i--;)
                    {
                        Point pointSource(negMitosis[i].x,negMitosis[i].y);
			//qDebug() << "nSource : " << negMitosis[i].x << negMitosis[i].y;
                        nSource.push_back(pointSource);
                    }

                    Mat thinPlateSplineImage;
                    //bool tpsOk = thinPlateSplineProcessing(imageCopy, thinPlateSplineImage, iP, iiP, pSource, nSource);

		    if (tpsOk)
		    { 
			thinPlateSplineProcessing(imageCopy, thinPlateSplineImage, iP, iiP, pSource, nSource);
			//cv::imwrite((destinationPath + fileName + "_TPS.jpg").toStdString(), thinPlateSplineImage);

                    	if ((int) nSource.size() > 0)
                    	{
                        	negMitosis.clear();
                        	for(int i = nSource.size(); i--;)
                        	{
                            		mitosis mitosisCoord;
                            		mitosisCoord.x = nSource[i].x;
					//qDebug() << "NegMitosis: " << mitosisCoord.x << mitosisCoord.y;
                            		mitosisCoord.y = nSource[i].y;
                            		mitosisCoord.grade = 0;
                            		negMitosis.push_back(mitosisCoord);
                        	}

                        	int negTPS = writeMitosisFiles(negMitosis, thinPlateSplineImage, negDir, fileName, Size(windowSize,windowSize));
                        	negativesMitosisCounter = negativesMitosisCounter + negTPS;
                    	}
		   }

                }

                //j++;

            }*/
	    qDebug() << "Total de negativos gerados: " << negativesMitosisCounter;

        //}
        qDebug() <<  "Total de positivos gerados: " << positivesMitosisCounter;
    }
    catch(const cv::Exception& ex)
    {
        std::cout << "Error: " << ex.what() << std::endl;
    }

    int nMilliseconds = timer->elapsed();
    qDebug() << "Tempo total de processamento: " << nMilliseconds << "milisegundos";

    return 0;

}

void rotateImage(Mat &src, Mat &dst, double angle)
{
    Point2f src_center(src.cols/2.0F, src.rows/2.0F);
    Mat r = getRotationMatrix2D(src_center, angle, 1.0);

    if (testGpu < 0)
    {
        cv::gpu::GpuMat dst_gpu, src_gpu;
        src_gpu.upload(src);
        //gpu::rotate(src_gpu,dst_gpu,Size(src_gpu.cols, src_gpu.rows), angle);
        gpu::warpAffine(src_gpu,dst_gpu,r, Size(src_gpu.cols-1, src_gpu.rows-1));
        dst_gpu.download(dst);
    }
    else
    {
        warpAffine(src, dst, r, Size(src.cols, src.rows));
    }

}

void flipImage(Mat &src, Mat &dst, int flipCode)
{
    if (testGpu < 0)
    {
        cv::gpu::GpuMat dst_gpu, src_gpu;
        src_gpu.upload(src);
        gpu::flip(src_gpu,dst_gpu,flipCode);
        dst_gpu.download(dst);
    }
    else
    {
        cv::flip(src,dst,flipCode);
    }

}

bool cropImage(Mat &src, Mat &croppedImage, Point coordinate, Size rectSize, int offset)
{
    cv::Mat dst(src);

    int y = 0;
    y = coordinate.y - offset;
    int x = 0;
    x = coordinate.x - offset;

    //qDebug() << offset;

    //qDebug() << coordinate.y << coordinate.x;
    //qDebug() << x << y;

/*    if (x + rectSize.width > src.cols)
    {
	int dx = (x + rectSize.width) - src.cols;
	x = x - dx -1;
	//qDebug() << x << dx;
    }

    if (x < 0)
	x =0;

    if (y + rectSize.height > src.rows)
    {
        int dy = (y + rectSize.height) - src.rows;
        y = y - dy -1;
	//qDebug() << y << dy;
    }

    if (y < 0)
	y=0;*/


    if (x >= 0 && y >= 0 && rectSize.width + x <= src.cols && rectSize.height + y <= src.rows)
    {
        cv::Rect rect = cv::Rect(x,y, rectSize.width, rectSize.height);
        croppedImage = dst(rect);
        return true;
    }

    else
    {
	//qDebug() << coordinate.y << coordinate.x;
    	//qDebug() << x << y;
        return false;
    }


}

void cropRect(images src, Point coordinate, Size rectSize, int offset, vector<mitosis> &mitos)
{
    QTime* timerCrop = new QTime();
    timerCrop->start();

    bool create = false;

    ///X
    int x = coordinate.x;
    ///Y
    int y = coordinate.y;

    if(x >= 0 && y >= 0 && rectSize.width + x < src.cols && rectSize.height + y < src.rows)
    {
        create = true;
    }

    if (create == true)
    {
        //Trabalhando com os quadrantes.
        mitosis mit;
        mit.grade = 0;

        //Q1
        mit.x = x + offset;
        mit.y = y + offset;
        if (mit.x - offset >=0 && mit.y - offset >=0)
            mitos.push_back(mit);

        //Q2
        mit.x = x + offset;
        mit.y = y - offset;
        if (mit.x - offset >=0 && mit.y - offset >=0)
            mitos.push_back(mit);

        //Q3
        mit.x = x - offset;
        mit.y = y + offset;
        if (mit.x - offset >=0 && mit.y - offset >=0)
            mitos.push_back(mit);

        //Q4
        mit.x = x - offset;
        mit.y = y - offset;
        if (mit.x - offset >=0 && mit.y - offset >=0)
            mitos.push_back(mit);

    }
}

vector<mitosis> expandMitosis(vector<mitosis> mitoses, images imageBase, Size size)
{
    vector<mitosis> mitos;

    ///Recorte da Imagem na região de interesse
    for (int j = mitoses.size(); j--;)
    {
        cropRect(imageBase, Point(mitoses[j].x,mitoses[j].y), size, gridWindow, mitos);
    }

    return mitos;
}

vector<Point> csvReaderPoints(QString fileName, const QString separator)
{
    vector<Point> pointVector;
    pointVector.clear();

    QFile file(fileName);
    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream textStream(&file);

        while( !textStream.atEnd() )
        {
            QString line = textStream.readLine();
            QStringList values = line.split(separator);

            double x = values[0].toDouble();
            double y = values[1].toDouble();

            Point imagePoint(x,y);
            pointVector.push_back(imagePoint);

        }
    }

    return pointVector;

}

vector<mitosis> csvReaderMitosis(QString fileName, const QString separator)
{
    vector<mitosis> pointVector;
    pointVector.clear();

    mitosis mitosisCoordinates;

    QFile file(fileName);
    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream textStream(&file);

        while( !textStream.atEnd() )
        {
            QString line = textStream.readLine();
            QStringList values = line.split(separator);

            mitosisCoordinates.x = values[0].toDouble();
            mitosisCoordinates.y = values[1].toDouble();
            mitosisCoordinates.grade = values[2].toDouble();

            pointVector.push_back(mitosisCoordinates);
        }
    }

    return pointVector;
}

vector<Point> csvReaderVectorPoints(QString fileName, const QString separator)
{
    vector<Point> vectorPointVector;
    vector<Point> pointVector;
    vectorPointVector.clear();

    QFile file(fileName);
    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream textStream(&file);

        while( !textStream.atEnd() )
        {
            pointVector.clear();

            QString line = textStream.readLine();
            QStringList values = line.split(separator);

            int vectorSize = values.size()/2;
            int vectorPairControl = 0;

            for (int i = 0; i < values.size(); i++)
            {
                if (vectorPairControl < vectorSize) {

                    double x = values[i].toDouble();
                    i++;
                    double y = values[i].toDouble();

                    Point imagePoint(x,y);
                    pointVector.push_back(imagePoint);

                    vectorPairControl++;

                } else {

                    continue;

                }

            }

            //cropROI(pointVector, image);
            cv::Point roiCenter;
            centerCropROI(pointVector, roiCenter);
            vectorPointVector.push_back(roiCenter);

        }
    }

    return vectorPointVector;

}

vector<mitosis> csvReaderVectorMitosis(QString fileName, const QString separator)
{
    vector<mitosis> vectorPointVector;
    vector<Point> pointVector;
    vectorPointVector.clear();

    QFile file(fileName);
    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream textStream(&file);

        while( !textStream.atEnd() )
        {
            pointVector.clear();

            QString line = textStream.readLine();
            QStringList values = line.split(separator);

            int vectorSize = values.size()/2;
            int vectorPairControl = 0;

            mitosis mitosisCoordinates;

            for (int i = 0; i < values.size(); i++)
            {
                if (vectorPairControl < vectorSize) {

                    double x = values[i].toDouble();
                    i++;
                    double y = values[i].toDouble();

                    Point imagePoint(x,y);
                    pointVector.push_back(imagePoint);

                    vectorPairControl++;

                } else {

                    continue;

                }
            }

            cv::Point roiCenter;
            centerCropROI(pointVector, roiCenter);
            mitosisCoordinates.x = roiCenter.x;
            mitosisCoordinates.y = roiCenter.y;
            mitosisCoordinates.grade = 0;
            vectorPointVector.push_back(mitosisCoordinates);

        }
    }

    return vectorPointVector;
}

int writeMitosisFiles(vector<mitosis> mitoses, Mat &imageBase, QString destinationPath, QString fileName, Size size)
{

    int countWritten = 0;

    vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(9);

    try 
    {
    	///Recorte da Imagem na regiao de interesse
    	for (int j = mitoses.size(); j--; )
    	{
           Mat imageCrop, imageFlip, imageFlipV90, imageRot90, imageFlipV180, imageRot180, imageFlipV270, imageRot270;

           ///Recorte da imagem a partir dos dados dos CSVs
           ///Imagem Original Recortada + Flip Vertical
           QString number = "_TPS" + QString::number(j);

           if (cropImage(imageBase, imageCrop, Point(mitoses[j].x,mitoses[j].y), size, window))
           {
             	cv::imwrite((destinationPath + fileName + number + ".png").toStdString(), imageCrop, compression_params);
            	flipImage(imageCrop, imageFlip, 1);
            	cv::imwrite((destinationPath + fileName + number + "_F0" + ".png").toStdString(), imageFlip, compression_params);

            	///Imagem Rotacionada 90 graus + Flip Vertical
            	rotateImage(imageCrop, imageRot90, 90.0);
            	cv::imwrite((destinationPath + fileName + number + "_R90" + ".png").toStdString(), imageRot90, compression_params);
            	flipImage(imageRot90, imageFlipV90, 1);
            	cv::imwrite((destinationPath + fileName + number + "_F90" + ".png").toStdString(), imageFlipV90, compression_params);

            	///Imagem Rotacionada 180 graus + Flip Vertical
            	rotateImage(imageCrop, imageRot180, 180.0);
            	cv::imwrite((destinationPath + fileName + number + "_R180" + ".png").toStdString(), imageRot180, compression_params);
            	flipImage(imageRot180, imageFlipV180, 1);
            	cv::imwrite((destinationPath + fileName + number + "_F180" + ".png").toStdString(), imageFlipV180, compression_params);

            	///Imagem Rotacionada 270 graus + Flip Vertical
            	rotateImage(imageCrop, imageRot270, 270.0);
            	cv::imwrite((destinationPath + fileName + number + "_R270" + ".png").toStdString(), imageRot270, compression_params);
            	flipImage(imageRot270, imageFlipV270, 1);
            	cv::imwrite((destinationPath + fileName + number + "_F270" + ".png").toStdString(), imageFlipV270, compression_params);

            	countWritten++;
            }
	    else
	    {
		qDebug() << "x: " << mitoses[j].x << "y :" << mitoses[j].y;
	    }

    	}
    }
    catch (cv::Exception& e)
    {
	const char* err_msg = e.what();
    	std::cout << "exception caught: " << err_msg << std::endl;
    }

    return countWritten;
}

vector<Point> createGrid(Mat &originalImage, int inc=0)
{
    vector<Point> pointVector;
    int cols = 5;
    int rows = 5;
    int colspacing = (originalImage.cols) / cols;
    int rowspacing = (originalImage.rows) / rows;
    //qDebug() << "###CreateGrid()";

    //qDebug() << "Colspacing = " << colspacing;
    //qDebug() << "Rowspacing = " << rowspacing;

    for (int i = 1; i < cols -1; i ++)
    {
	for (int j = 1; j < rows -1; j++)
        {
            int x = colspacing*(i+1);
            int y = rowspacing*(j+1);
            //1539x1376
            if ((x >= 0 && x <= originalImage.cols/2) && (y >= 0 && y <= originalImage.rows/2))
            {
            	x = x + inc;
            	y = y + inc;
            }
            if ((x <= originalImage.cols && x >= originalImage.cols/2) && (y >= 0 && y <= originalImage.rows/2))
            {
            	x = x - inc;
            	y = y + inc;
            }
            if ((x >= 0 && x <= originalImage.cols/2) && (y <= originalImage.rows && y >= originalImage.rows/2))
            {
            	x = x + inc;
            	y = y - inc;
            }
            if ((x <= originalImage.cols && x >= originalImage.cols/2) && (y <= originalImage.rows && y >= originalImage.rows/2))
            {
            	x = x - inc;
            	y = y - inc;
            }

            Point tmpPoint = Point(x, y);
            //qDebug() << "Point: " << x << y;
            pointVector.push_back(tmpPoint);
	}
    }

    return pointVector;
}

vector<Point> generateGrid(Mat &originalImage, int cols, int rows, float inc=0)
{
    vector<Point> pointVector;
    int colspacing = (originalImage.cols) / cols;
    int rowspacing = (originalImage.rows) / rows;

    //qDebug() << colspacing << rowspacing << cols << rows;
    //++ -- +- -+

    Point tmpPoint = Point(0,0);

    for (int i = 0; i < cols; i ++)
    {
	//if (tmpPoint.x < originalImage.cols && tmpPoint.x + colspacing + inc < originalImage.cols)
            tmpPoint.x = tmpPoint.x + colspacing + inc;
	//else
	    //tmpPoint.x = tmpPoint.x + inc;

	for (int j = 0; j < rows; j++)
        {
	    //if (tmpPoint.y < originalImage.rows && tmpPoint.y + rowspacing + inc < originalImage.rows)
                tmpPoint.y = tmpPoint.y + rowspacing + inc;
	    //else
		//tmpPoint.y = tmpPoint.y + inc;
 
	   //qDebug() << tmpPoint.x << tmpPoint.y;
	   // if (tmpPoint.y < originalImage.rows && tmpPoint.x < originalImage.cols)
           pointVector.push_back(tmpPoint);
        }
	tmpPoint.y = 0;
    }

    //qDebug() << QString::number(pointVector.size());

    return pointVector;
}

vector<Point> generateMitosisGrid(Mat &originalImage, vector<mitosis> mitos, float inc=0)
{
    vector<Point> pointVector;
    int colspacing = 50;
    int rowspacing = 50;

    //Margin
    Point pointc1 = Point(colspacing,rowspacing);
    pointVector.push_back(pointc1);
    Point pointc2 = Point((originalImage.cols - colspacing), rowspacing);
    pointVector.push_back(pointc2);
    Point pointc3 = Point(colspacing, (originalImage.rows - rowspacing));
    pointVector.push_back(pointc3);
    Point pointc4 = Point((originalImage.cols - colspacing), (originalImage.rows - rowspacing));
    pointVector.push_back(pointc4);

    for (int i = 0; i < mitos.size(); i ++)
    {
	int x = mitos[i].x;
	int y = mitos[i].y;
	//1539x1376
	if ((x >= 0 && x <= originalImage.cols/2) && (y >= 0 && y <= originalImage.rows/2))
	{
	    x = x + inc;
	    y = y + inc;
	}
	if ((x <= originalImage.cols && x >= originalImage.cols/2) && (y >= 0 && y <= originalImage.rows/2))
	{
	    x = x - inc;
	    y = y + inc;
	}
	if ((x >= 0 && x <= originalImage.cols/2) && (y <= originalImage.rows && y >= originalImage.rows/2))
	{
	    x = x + inc;
	    y = y - inc;
	}
	if ((x <= originalImage.cols && x >= originalImage.cols/2) && (y <= originalImage.rows && y >= originalImage.rows/2))
	{
	    x = x - inc;
	    y = y - inc;
	}
        Point tmpPoint = Point(x, y);
	//qDebug() << "Point: " << x << y;
 	pointVector.push_back(tmpPoint);
    }
    return pointVector;
}


vector<Point> changeGrid(vector<Point> pointVector, vector<mitosis> mitosis)
{
    try
    {
    	Point tmpPoint = Point(0, 0);

    	for (int l = mitosis.size(); l--;)
    	{
           int x = mitosis[l].x;
           int y = mitosis[l].y;
           tmpPoint.x = x;
           tmpPoint.y = y;
           pointVector.push_back(tmpPoint);

           int pos = std::find(pointVector.begin(), pointVector.end(), tmpPoint) - pointVector.begin();
           if (pos > 0 && pos < (int)pointVector.size())
	   {
                double posL1 = calculateDistance(pointVector[pos], pointVector[pos-1]);
            	double posP1 = calculateDistance(pointVector[pos], pointVector[pos+1]);

                if (posL1 < posP1)
                    pointVector.erase(pointVector.begin()+pos-1);
                else
                    pointVector.erase(pointVector.begin()+pos+1);
           }
           else
	   {
            	if (pos == 0)
                    pointVector.erase(pointVector.begin()+pos+1);
            	if (pos == (int) pointVector.size())
                    pointVector.erase(pointVector.begin()+pos-1);
           }
    	}
    }
    catch( cv::Exception& e )
    {
    	const char* err_msg = e.what();
    	std::cout << "exception caught: " << err_msg << std::endl;
    }

    return pointVector;
}

double calculateDistance(const Point& pt1, const Point& pt2)
{
    double deltaX = pt1.x - pt2.x;
    double deltaY = pt1.y - pt2.y;
    return sqrt(pow(deltaX,2) + pow(deltaY,2));
}

void cropROI(vector<Point> points, Mat &image, Mat &crop)
{
    try
    {
    	/// From the points, figure out the size of the ROI
    	int left, right, top, bottom;
    	for (int i = points.size(); i--;)
    	{
            if (i == 0) /// initialize corner values
            {
            	left = right = points[i].x;
            	top = bottom = points[i].y;
            }

            if (points[i].x < left)
            	left = points[i].x;

            if (points[i].x > right)
            	right = points[i].x;

            if (points[i].y < top)
    	        top = points[i].y;

            if (points[i].y > bottom)
            	bottom = points[i].y;
	}

    	std::vector<cv::Point> box_points;
	box_points.clear();
    	box_points.push_back(cv::Point(left, top));
    	box_points.push_back(cv::Point(left, bottom));
    	box_points.push_back(cv::Point(right, bottom));
    	box_points.push_back(cv::Point(right, top));

    	/// Compute minimal bounding box for the ROI
    	/// Note: for some unknown reason, width/height of the box are switched.
    	cv::RotatedRect box = cv::minAreaRect(cv::Mat(box_points));
    	//std::cout << "box w:" << box.size.width << " h:" << box.size.height << std::endl;

    	/// Set the ROI to the area defined by the box
    	/// Note: because the width/height of the box are switched,
    	/// they were switched manually in the code below:
    	cv::Rect roi;
    	roi.x = box.center.x - (box.size.height / 2);
    	roi.y = box.center.y - (box.size.width / 2);
    	roi.width = box.size.height;
    	roi.height = box.size.width;
    	//std::cout << "roi @ " << roi.x << "," << roi.y << " " << roi.width << "x" << roi.height << std::endl;

    	/// Crop the original image to the defined ROI
    	crop = image(roi);
    }
    catch( cv::Exception& e )
    {
    	const char* err_msg = e.what();
    	std::cout << "exception caught: " << err_msg << std::endl;
    }
}

void centerCropROI(vector<Point> points, Point &roiCenter)
{
    try
    {
    	/// From the points, figure out the size of the ROI
    	int left, right, top, bottom;
    	for (int i = points.size(); i--;)
    	{
            if (i == 0) /// initialize corner values
            {
            	left = right = points[i].x;
            	top = bottom = points[i].y;
            }

            if (points[i].x < left)
            	left = points[i].x;

            if (points[i].x > right)
            	right = points[i].x;

            if (points[i].y < top)
            	top = points[i].y;

            if (points[i].y > bottom)
    	        bottom = points[i].y;
    	}

    	std::vector<cv::Point> box_points;
	box_points.clear();
    	box_points.push_back(cv::Point(left, top));
    	box_points.push_back(cv::Point(left, bottom));
    	box_points.push_back(cv::Point(right, bottom));
    	box_points.push_back(cv::Point(right, top));

    	/// Compute minimal bounding box for the ROI
    	/// Note: for some unknown reason, width/height of the box are switched.
    	cv::RotatedRect box = cv::minAreaRect(cv::Mat(box_points));
    	//std::cout << "box w:" << box.size.width << " h:" << box.size.height << std::endl;

    	/// Set the ROI to the area defined by the box
    	/// Note: because the width/height of the box are switched,
    	/// they were switched manually in the code below:
    	cv::Rect roi;
    	roi.x = box.center.x - (box.size.height / 2);
    	roi.y = box.center.y - (box.size.width / 2);
    	roi.width = box.size.height;
    	roi.height = box.size.width;
    	//std::cout << "roi @ " << roi.x << "," << roi.y << " " << roi.width << "x" << roi.height << std::endl;

    	roiCenter.x = roi.x;
    	roiCenter.y = roi.y;
    	//qDebug() << "x:" << roiCenter.x;
    	//qDebug() << "y:" <<roiCenter.y;
    }
    catch( cv::Exception& e )
    {
    	const char* err_msg = e.what();
	std::cout << "CenterCropROI()" << std::endl;
    	std::cout << "exception caught: " << err_msg << std::endl;
    }
}

