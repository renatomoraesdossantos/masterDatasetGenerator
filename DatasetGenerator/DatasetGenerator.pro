TEMPLATE = app
#CONFIG += console

QMAKE_CXXFLAGS += -std=c++11

CONFIG -= app_bundle
CONFIG += qt

QT += core gui

SOURCES += main.cpp \
    imagedata.cpp \
    filedata.cpp \
    ThinPlateSpline/CThinPlateSpline.cpp \
    ThinPlateSpline/ThinPlateSpline.cpp

include(deployment.pri)
qtcAddDeployment()

#INCLUDEPATH += /usr/local/include/

INCLUDEPATH += OpenCVLibs/include/
LIBS += `pkg-config --static --cflags --libs opencv`

INCLUDEPATH += "log4cpp"
LIBS += -L"log4cpp/lib"
LIBS += -llog4cpp


#LIBS = -Wl,-rpath,/home/renatosantos/Script/OpenCVLibs/libs/
#LIBS += -L"/home/renatosantos/Script/OpenCVLibs/libs/"

#LIBS += -L"/usr/local/lib/"
#LIBS += -lopencv_highgui
#LIBS += -lopencv_imgproc
#LIBS += -lopencv_core
#LIBS += -lopencv_gpu
#LIBS += -lopencv_imgcodecs

HEADERS += \
    imagedata.h \
    filedata.h \
    ThinPlateSpline/constants.h \
    ThinPlateSpline/CThinPlateSpline.h \
    ThinPlateSpline/ThinPlateSpline.h
