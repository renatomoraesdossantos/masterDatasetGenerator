#include <stdio.h>
#include <iostream>
#include <QDir>
#include <QDirIterator>
#include <QDebug>
#include <QCoreApplication>
#include <QFile>
#include <QStringList>
#include <opencv2/opencv.hpp>
#include <QTimer>
#include <QTime>
#include <QVector>

using namespace cv;
using namespace std;

/**
 * @brief The mitosisFile struct
 */
struct imageFile {
    QString fileAbsolutePath;
    QString fileName;
    QString fileExtension;
};

inline QString withoutExtension(const QString & fileName) {
    return fileName.left(fileName.lastIndexOf("."));
}

inline QString onlyExtension(const QString & fileName) {
    return fileName.mid(fileName.lastIndexOf("."),fileName.length());
}

int main(int argc, char** argv)
{

    QCoreApplication app(argc, argv);

    QTime* timer = new QTime();
    timer->start();

    try
    {
        ////Paths A06  A08  A09  A13  A16
        const static QString sourcePath = "/home/renatosantos/MITOS-APERIO-x40/orig/test/Scanner_Aperio/A16/frames/x40/";
	////Paths A00 A01 A02 A03 A04
	//const static QString sourcePath = "/home/renatosantos/MITOS/TrainingDataSet/ScannerA/A04_v2/";
	////Paths A00_v2 A01_v2 A02_v2 A03_v2 A04_v2
	///const static QString sourcePath ="/home/renatosantos/MITOS/TestingDataSet/mitosis_evaluation_set_A/A04_v2/";
        //const static QString destinationPath = "/data/renatosantos/datasets/test_mitos/A04/";
	const static QString destinationPath = "/data/renatosantos/datasets/atypia14/A16_v3/";

        QDir directory(sourcePath);
        if (!directory.exists()) {
           cout << "The directory "+ sourcePath.toStdString() + " not exist.";
           return 0;
        }

        ///Root destination directory
        QDir destinationDirectory(destinationPath);
        destinationDirectory.mkdir(destinationPath);

        QStringList filters;
        filters << "*.png" << "*.jpg" << "*.bmp" << "*.tiff" << "*.gif";

        directory.setFilter(QDir::Files|QDir::NoDotAndDotDot);
        directory.setNameFilters(filters);

        QDirIterator directories(directory, QDirIterator::Subdirectories);

        QTime* timerReadingDir = new QTime();
        timerReadingDir->start();

        int N = 101;

	QString filename = "/data/renatosantos/datasets/atypia14/A16_v3.txt";
    	QFile file(filename);
    	if (file.open(QIODevice::ReadWrite)) {
            QTextStream stream(&file);
            //stream << "#A06" << endl;


        while (directories.hasNext())
        {
            QString fileAbsolutePath = directories.next();
            cv::Mat img = cv::imread(fileAbsolutePath.toStdString());
	    QFileInfo name = directories.fileInfo();
	    

            //for (int r = 0; r < img.rows; r += N/2)
	    for (int r = 0; r < img.rows; r++)
            {
		//for (int c = 0; c < img.cols; c += N/2)
                for (int c = 0; c < img.cols; c++)
                {
                    cv::Mat tile = img(cv::Range(r, min(r + N, img.rows)),cv::Range(c, min(c + N, img.cols)));//no data copying here
                    //cv::Mat tileCopy = img(cv::Range(r, min(r + N, img.rows)),
                                 //cv::Range(c, min(c + N, img.cols))).clone();//with data copying
		    QString number = "_" + QString::number(r) + "_" + QString::number(c);
		    //int x = c + N/2;
		    //int y = r + N/2;

		    QString nameTest = destinationPath + name.baseName() + number + ".jpg";
		    //qDebug() << nameTest;
		    stream << nameTest /*<< "  0"*/<< endl;

                    imwrite(nameTest.toStdString(), tile );
                }
            }

	   /*for (int r = img.rows; 0 < r; r -= N)
            {
                for (int c = img.cols; 0 < c; c -= N)
                {
                    cv::Mat tile = img(cv::Range(r, min(r + N, img.rows)),
                                 cv::Range(c, min(c + N, img.cols)));//no data copying here
                    //cv::Mat tileCopy = img(cv::Range(r, min(r + N, img.rows)),
                                 //cv::Range(c, min(c + N, img.cols))).clone();//with data copying
                    QString number = "_" + QString::number(r) + "_" + QString::number(c);

                    QString nameTest = destinationPath + name.baseName() + number + ".jpg";
                    //qDebug() << nameTest;
		    stream << nameTest << " 0" << endl;

                    imwrite(nameTest.toStdString(), tile );
                }
            }*/
        }
        qDebug() << "Tempo Leitura Diretorios: " << timerReadingDir->elapsed() << "milisegundos";
    }

    file.close();


    }
    catch(const cv::Exception& ex)
    {
        std::cout << "Error: " << ex.what() << std::endl;
    }

    int nMilliseconds = timer->elapsed();
    qDebug() << "Tempo total de processamento: " << nMilliseconds << "milisegundos";

    return 0;

}

