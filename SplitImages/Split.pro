#TEMPLATE = app
#CONFIG += console

#CONFIG += c++11
QMAKE_CXXFLAGS += -std=c++11

CONFIG -= app_bundle
CONFIG += qt

#CONFIG += staticlib

QT += core gui

SOURCES += \
    main.cpp

#include(deployment.pri)
#qtcAddDeployment()

INCLUDEPATH += /usr/local/include/opencv2
LIBS += `pkg-config opencv --libs`

#LIBS += `pkg-config --static --cflags --libs opencv`
#LIBS += -L"/usr/local/lib"

#LIBS += -lopencv_highgui
#LIBS += -lopencv_imgproc
#LIBS += -lopencv_core
#LIBS += -lopencv_gpu

#LIBS += -L"/usr/local/cuda/lib64/"

